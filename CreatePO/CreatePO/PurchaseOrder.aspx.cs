﻿using CreatePO.PurchaseOrderHeaderService;
using CreatePO.PurchaseOrderItemService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreatePO.GatewayAuthService;
using System.Data;
using iTECH.Library.Utilities;

namespace CreatePO
{
    public partial class PurchaseOrder : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Testing
                //POTransaction objPOTrns = new POTransaction();
                //DataTable dt = new DataTable();
                //dt = objPOTrns.PurchaseOrderItemData("91695");

                GatewayAuthService.AuthenticationToken currentToken = new GatewayAuthService.AuthenticationToken();
                currentToken = UserAuthentication();
                if (currentToken.value.Length > 10)
                {
                    try
                    {
                        //CreatePurchaseOrderHdrAutoNumbering(currentToken);
                        CreatePurchaseOrderHdrManualNumbering(currentToken);
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.createLog("Purchase Order Creation error: " + ex.Message);
                        UserAuthenticationLogOut(currentToken);
                    }
                }
                else
                { ErrorLog.createLog("User Authentication failed Token not generate. error: "); }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("User Authentication failed Token not generate. error: " + ex.Message);
            }

        }

        public GatewayAuthService.AuthenticationToken UserAuthentication()
        {
            GatewayAuthService.AuthenticationToken returnToken = new GatewayAuthService.AuthenticationToken();
            GatewayAuthService.GatewayLoginResponse o = new GatewayAuthService.GatewayLoginResponse();
            string sTokenValue = "";
            try
            {
                GatewayAuthService.GatewayLoginRequestType g = new GatewayAuthService.GatewayLoginRequestType();
                //g.companyId = "USS";
                //g.connectedAccessType = "I";
                //g.environmentClass = "T01";
                //g.environmentName = "tstusstx";
                //g.forceDisconnect = true;
                //g.username = "itech2";
                //g.password = "password";

                g.companyId = "USS";
                g.connectedAccessType = "I";
                g.environmentClass = "LIV";
                g.environmentName = "livusstx";
                g.forceDisconnect = true;
                g.username = "itech2";
                g.password = "password";

                GatewayAuthService.GatewayLoginRequest ga = new GatewayAuthService.GatewayLoginRequest(g);



                GatewayAuthService.AuthenticationService a = new GatewayAuthService.AuthenticationServiceClient();

                o = a.GatewayLogin(ga);

                String sTokenUser = o.response.authenticationToken.username;
                sTokenValue = o.response.authenticationToken.value;

                returnToken = o.response.authenticationToken;

                //Write log for success
                //ErrorLog.createLog("User authentication success " + "\n sTokenUser: " + sTokenUser + "\n sTokenValue: " + sTokenValue + "\n returnToken: " + returnToken);
            }
            catch (Exception MyError)
            {
                ErrorLog.createLog("User authentication error: " + o.ServiceMessagesHeader.messageList[0].ToString() + "\n" + MyError.Message);

                throw;
            }
            return returnToken;

        }
        public void UserAuthenticationLogOut(GatewayAuthService.AuthenticationToken iToken)
        {
            try
            {
                GatewayAuthService.AuthenticationService a = new GatewayAuthService.AuthenticationServiceClient();

                GatewayAuthService.LogoutRequest lo = new GatewayAuthService.LogoutRequest();
                lo.authenticationToken = iToken;

                GatewayAuthService.LogoutResponse lr = new GatewayAuthService.LogoutResponse();

                lr = a.Logout(lo);

            }
            catch (Exception MyError)
            {
                ErrorLog.createLog("User authentication LogOut error: " + MyError.Message);

            }
        }

        //public void CreatePurchaseOrderHdrAutoNumbering(GatewayAuthService.AuthenticationToken iToken)
        //{
        //    try
        //    {
        //        POTransaction objPOTrans = new POTransaction();
        //        DataTable dtPOH = new DataTable();
        //        DataTable dtPOItm = new DataTable();
        //        DataTable dtPORls = new DataTable();
        //        DataTable dtPODtl = new DataTable();
        //        DataTable dtPOPrdItm = new DataTable();

        //        try
        //        {
        //            dtPOH = objPOTrans.PurchaseHeaderData();
        //        }
        //        catch (Exception ex)
        //        {
        //            ErrorLog.createLog("Error during geting fetching data from potpoh table from database " + ex.Message);
        //            throw;
        //        }

        //        //Headers objects
        //        PurchaseOrderHeaderService.PurchaseOrder objPOType;
        //        PurchaseOrderHeaderService.CreateWithAutoNumberingRequest oCreateWithAutoNumberingRequest;
        //        PurchaseOrderHeaderService.CreateWithAutoNumberingResponse oCreateWithAutoNumberingResponse;
        //        PurchaseOrderHeaderService.PurchaseOrderService oPurchaseOrderServiceClient;
        //        PurchaseOrderHeaderService.AuthenticationToken objPOHdrAuth;

        //        //Gatway Authentication token is same for all PO header
        //        objPOHdrAuth = new PurchaseOrderHeaderService.AuthenticationToken();
        //        objPOHdrAuth.username = iToken.username;
        //        objPOHdrAuth.value = iToken.value;

        //        // PO Header Processing directory is common for all PO headers
        //        PurchaseOrderHeaderService.ProcessingDirectives objPrsDrc = new PurchaseOrderHeaderService.ProcessingDirectives();
        //        objPrsDrc.useDefaultHeaderInformation = true;
        //        objPrsDrc.useDefaultShipFromNameAndAddress = true;
        //        objPrsDrc.useDefaultSalesContact = true;
        //        objPrsDrc.useDefaultFreight = true;
        //        objPrsDrc.useDefaultInstructions = true;
        //        objPrsDrc.useDefaultAuthorizationAndTransactionStatus = true;

        //        // PO Items Objects
        //        PurchaseOrderItemService.Release objRlsType;
        //        PurchaseOrderItemService.ItemProduct objItmPrdType;
        //        PurchaseOrderItemService.OrderItem objPOItmType;
        //        PurchaseOrderItemService.Distribution objDstbType;
        //        PurchaseOrderItemService.AuthenticationToken oAuthenticationToken;

        //        //PO Items Processing directory is common for all PO Items
        //        PurchaseOrderItemService.ProcessingDirectives objPrsDrcType = new PurchaseOrderItemService.ProcessingDirectives();
        //        objPrsDrcType.useDefaultDetailInformation = true;
        //        objPrsDrcType.useDefaultInstructions = true;
        //        objPrsDrcType.useDefaultTransactionStatus = true;
        //        objPrsDrcType.useDefaultProductDescriptions = true;
        //        objPrsDrcType.useDefaultConditions = true;
        //        objPrsDrcType.useDefaultTolerances = true;
        //        objPrsDrcType.useDefaultCoilPackagings = true;
        //        objPrsDrcType.useDefaultPackaging = true;
        //        objPrsDrcType.useDefaultStandardSpecs = true;
        //        objPrsDrcType.useDefaultTests = true;
        //        objPrsDrcType.useDefaultChemicalSpecifications = true;
        //        objPrsDrcType.useDefaultTestCertificates = true;
        //        objPrsDrcType.useDefaultOriginZones = true;

        //        foreach (DataRow drPOH in dtPOH.Rows)
        //        {
        //            //Start creating purchase order header table from table potpoh 

        //            objPOType = new PurchaseOrderHeaderService.PurchaseOrder();
        //            objPOType.identity = "";
        //            objPOType.orderPrefix = "PO";
        //            objPOType.orderNumber = 0;
        //            objPOType.orderBranchId = "LAX";//BusinessUtility.GetString(drPOH[1]);
        //            objPOType.vendorId = "L" + BusinessUtility.GetString(drPOH[2]);//"L-1192";//poh_ven_id // Prefix "L-" is medatory from table aprven_rec where ven_ven_id like  '%1210'
        //            objPOType.shipFromId = BusinessUtility.GetInt(drPOH[3]); //poh_shp_fm
        //            objPOType.millId = BusinessUtility.GetString(drPOH[4]); //poh_mill
        //            objPOType.orderType = BusinessUtility.GetString(drPOH[5]); //poh_po_typ
        //            objPOType.invoiceMailToBranchId = "LAX"; //BusinessUtility.GetString(drPOH[6]);
        //            objPOType.paymentTermCode = BusinessUtility.GetInt(drPOH[7]); //poh_pur_pttrm
        //            objPOType.discountTermCode = BusinessUtility.GetInt(drPOH[8]); //poh_pur_disc_trm
        //            objPOType.buyerId = BusinessUtility.GetString(drPOH[9]);//"JE"; //poh_byr
        //            objPOType.currencyCode = BusinessUtility.GetString(drPOH[10]);//"USD"; //poh_cry
        //            objPOType.exchangeRate = BusinessUtility.GetDecimal(drPOH[11]); //poh_exrt
        //            objPOType.exchangeRateType = BusinessUtility.GetString(drPOH[12]);//"V";  //poh_ex_rt_typ
        //            objPOType.shipItemsTogether = BusinessUtility.GetBool(drPOH[13]); //poh_shp_itm_tgth
        //            objPOType.acknowledgementRequired = BusinessUtility.GetBool(drPOH[14]); //poh_ack_reqd
        //            objPOType.orderPlacedDate = BusinessUtility.GetDateTime(drPOH[15]);//BusinessUtility.GetDateTime("2015/03/23", DateFormat.yyyyMMdd);   // poh_po_pl_dt //New
        //            //objPOType.purchaseOrderIdentity = "";

        //            oCreateWithAutoNumberingRequest = new PurchaseOrderHeaderService.CreateWithAutoNumberingRequest(objPOHdrAuth, objPOType, objPrsDrc);

        //            oCreateWithAutoNumberingResponse = new PurchaseOrderHeaderService.CreateWithAutoNumberingResponse();

        //            oPurchaseOrderServiceClient = new PurchaseOrderHeaderService.PurchaseOrderServiceClient();

        //            // PO Header Identtity is generate
        //            oCreateWithAutoNumberingResponse = oPurchaseOrderServiceClient.CreateWithAutoNumbering(oCreateWithAutoNumberingRequest);

        //            //ErrorLog.createLog("POHeader Token: " + oCreateWithAutoNumberingResponse.AuthenticationHeader.value + "\n POHeader Token User: " + oCreateWithAutoNumberingResponse.AuthenticationHeader.username
        //            //    + "\n" + "PO Header Identity:" + oCreateWithAutoNumberingResponse.purchaseOrderIdentity);

        //            oAuthenticationToken = new PurchaseOrderItemService.AuthenticationToken();
        //            oAuthenticationToken.username = oCreateWithAutoNumberingResponse.AuthenticationHeader.username;
        //            oAuthenticationToken.value = oCreateWithAutoNumberingResponse.AuthenticationHeader.value;

        //            //Start creating Purchase order items 

        //            // Get data from PO Item table potpoi
        //            try
        //            {
        //                dtPOItm = objPOTrans.PurchaseOrderItemData(BusinessUtility.GetString(drPOH[0]));
        //            }
        //            catch (Exception ex)
        //            {
        //                ErrorLog.createLog("Error during geting fetching data from potpoi table from database for PO no. " + BusinessUtility.GetString(drPOH[0]) +
        //                    "\n" + ex.Message);
        //               // throw;
        //            }


        //            // Get data from PO Item table potpor
        //            try
        //            {
        //                dtPORls = objPOTrans.PurchaseOrderRlsData(BusinessUtility.GetString(drPOH[0]));
        //            }
        //            catch (Exception ex)
        //            {
        //                ErrorLog.createLog("Error during geting fetching data from potpor table from database for PO no. " + BusinessUtility.GetString(drPOH[0]) +
        //                    "\n" + ex.Message);
        //               // throw;
        //            }


        //            // Get data from PO Item table potpod
        //            try
        //            {
        //                dtPODtl = objPOTrans.PurchaseOrderDistData(BusinessUtility.GetString(drPOH[0]));
        //            }
        //            catch (Exception ex)
        //            {
        //                ErrorLog.createLog("Error during geting fetching data from potpod table from database for PO no. " + BusinessUtility.GetString(drPOH[0]) +
        //                    "\n" + ex.Message);
        //               // throw;
        //            }

        //            // Get data from PO Item table tctipd
        //            try
        //            {
        //                dtPOPrdItm = objPOTrans.PurchaseOrderPrdItm(BusinessUtility.GetString(drPOH[0]));
        //            }
        //            catch (Exception ex)
        //            {
        //                ErrorLog.createLog("Error during geting fetching data from tctipd table from database for PO no. " + BusinessUtility.GetString(drPOH[0]) +
        //                    "\n" + ex.Message);
        //               // throw;
        //            }

        //            if (dtPOItm.Rows.Count == dtPORls.Rows.Count && dtPOItm.Rows.Count == dtPODtl.Rows.Count && dtPODtl.Rows.Count == dtPOPrdItm.Rows.Count)
        //            {
        //                for (int i = 0; i < dtPOItm.Rows.Count; i++)
        //                {
        //                    // Set PO Release values
        //                    objRlsType = new PurchaseOrderItemService.Release();
        //                    //objRlsType.identity = "";
        //                    //objRlsType.refPrefix = "PO"; //por_po_pfx //New
        //                    //objRlsType.refNumber = 222;
        //                    //objRlsType.refItemNumber = 586;
        //                    objRlsType.internalReleaseNumber = BusinessUtility.GetInt(dtPORls.Rows[i][0]);//1; //por_po_intl_rls_no //
        //                    objRlsType.releaseNumber = BusinessUtility.GetString(dtPORls.Rows[i][1]); //por_po_rls_no //
        //                    objRlsType.readyPieces = BusinessUtility.GetInt(dtPORls.Rows[i][2]); //por_rdy_pcs
        //                    objRlsType.readyMeasure = BusinessUtility.GetDecimal(dtPORls.Rows[i][3]); //por_rdy_msr
        //                    objRlsType.readyWeight = BusinessUtility.GetDecimal(dtPORls.Rows[i][4]); //por_rdy_wgt
        //                    objRlsType.readyQuantity = BusinessUtility.GetInt(dtPORls.Rows[i][5]); //por_rdy_qty
        //                    objRlsType.readyDate = BusinessUtility.GetDateTime(dtPORls.Rows[i][6]); //por_rdy_dt
        //                    objRlsType.defaultDueDateQualifier = BusinessUtility.GetString(dtPORls.Rows[i][7]); //por_due_dt_qlf
        //                    // its showing error in table sctmsg "Due Date is invalid" when defaultDueDateFormat = "D" For now we are passing  "T" values
        //                    //objRlsType.defaultDueDateFormat = BusinessUtility.GetString(dtPORls.Rows[i][8]);//"T"; //por_due_dt_fmt 
        //                    objRlsType.defaultDueDateFormat = "T";
        //                    if (objRlsType.defaultDueDateFormat != "T")
        //                        objRlsType.dueDateEntry = BusinessUtility.GetInt(dtPORls.Rows[i][9]);//Blank when defaultDueDateFormat = "T"; // por_due_dt_ent

        //                    objRlsType.dueFromDate = BusinessUtility.GetDateTime(dtPORls.Rows[i][10].ToString(), DateFormat.MMddyyyy); //por_due_fm_dt //New //10/15/2014
        //                    objRlsType.dueToDate = BusinessUtility.GetDateTime(dtPORls.Rows[i][11].ToString(), DateFormat.MMddyyyy); //por_due_to_dt
        //                    objRlsType.dueDateWeekNo = BusinessUtility.GetInt(dtPORls.Rows[i][12]); //por_due_dt_wk_no //New
        //                    objRlsType.dueDateYear = BusinessUtility.GetInt(dtPORls.Rows[i][13]); //por_due_dt_wk_cy //New
        //                    objRlsType.rollingSchedule = BusinessUtility.GetString(dtPORls.Rows[i][14]); //por_roll_sch
        //                    objRlsType.acknowledgementReceived = BusinessUtility.GetBool(dtPORls.Rows[i][15]);//false;
        //                    if (objRlsType.acknowledgementReceived == true)
        //                        objRlsType.acknowledgementDate = BusinessUtility.GetDateTime(dtPORls.Rows[i][16]); // Must set value when objRlsType.acknowledgementReceived = true //por_ack_dt
        //                    //objRlsType.millOrderNumber = "";


        //                    objItmPrdType = new PurchaseOrderItemService.ItemProduct();
        //                    objItmPrdType.formId = BusinessUtility.GetString(dtPOPrdItm.Rows[i][0]); //"SSRD"; //ipd_frm
        //                    objItmPrdType.gradeId = BusinessUtility.GetString(dtPOPrdItm.Rows[i][1]);//"13-8"; //ipd_grd
        //                    objItmPrdType.numericSize1 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][2]); //1; //ipd_num_size1
        //                    objItmPrdType.numericSize1Specified = true;
        //                    objItmPrdType.numericSize2 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][3]);
        //                    objItmPrdType.numericSize2Specified = true;
        //                    objItmPrdType.numericSize3 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][4]);
        //                    objItmPrdType.numericSize3Specified = true;
        //                    objItmPrdType.numericSize4 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][5]);
        //                    objItmPrdType.numericSize4Specified = true;
        //                    objItmPrdType.numericSize5 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][6]);
        //                    objItmPrdType.numericSize5Specified = true;
        //                    objItmPrdType.sizeId = BusinessUtility.GetString(dtPOPrdItm.Rows[i][7]); //"1"; //ipd_size
        //                    objItmPrdType.finishId = BusinessUtility.GetString(dtPOPrdItm.Rows[i][8]);

        //                    //string[] stExtendFns = new string[] { "US", "2.5" };
        //                    //objItmPrdType.extendedFinish = stExtendFns;
        //                    objItmPrdType.width = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][9]); //ipd_wdth
        //                    objItmPrdType.widthSpecified = true;
        //                    objItmPrdType.length = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][10]); //144; //ipd_lgth
        //                    objItmPrdType.lengthSpecified = true;
        //                    objItmPrdType.dimensionDesignator = BusinessUtility.GetString(dtPOPrdItm.Rows[i][11]); //"F"; //ipd_dim_dsgn
        //                    objItmPrdType.innerDiameter = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][12]); //ipd_idia
        //                    objItmPrdType.outerDiameter = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][13]);  //ipd_odia
        //                    //objItmPrdType.wallThickness = 0.0M;
        //                    objItmPrdType.gaugeSize = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][14]); //ipd_ga_size
        //                    objItmPrdType.gaugeType = BusinessUtility.GetString(dtPOPrdItm.Rows[i][15]);//"N"; //ipd_ga_typ
        //                    objItmPrdType.alternateSize = BusinessUtility.GetString(dtPOPrdItm.Rows[i][16]);  //ipd_alt_size
        //                    objItmPrdType.randomDimension1 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][17]); //ipd_rdm_dim_1
        //                    objItmPrdType.randomDimension2 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][18]); //ipd_rdm_dim_2
        //                    objItmPrdType.randomDimension3 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][19]); //ipd_rdm_dim_3
        //                    objItmPrdType.randomDimension4 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][20]);//ipd_rdm_dim_4
        //                    objItmPrdType.randomDimension5 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][21]); //ipd_rdm_dim_5
        //                    objItmPrdType.randomDimension6 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][22]); //ipd_rdm_dim_6
        //                    objItmPrdType.randomDimension7 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][23]); //ipd_rdm_dim_7
        //                    objItmPrdType.randomDimension8 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][24]);//ipd_rdm_dim_8
        //                    if (objItmPrdType.dimensionDesignator != "M" && objItmPrdType.dimensionDesignator != "F")
        //                    {
        //                        objItmPrdType.randomArea = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][25]);//Ignored if dimensionDesignator = "M" or "F" //ipd_rdm_area
        //                    }
        //                    objItmPrdType.customerVendorId = "L" + BusinessUtility.GetString(dtPOPrdItm.Rows[i][26]); //"L-1039"; //ipd_cus_ven_id
        //                    objItmPrdType.entryMeasure = BusinessUtility.GetString(dtPOPrdItm.Rows[i][27]);//"E"; //ipd_ent_msr
        //                    objItmPrdType.lengthInFeetOrMeters = false; //need to discuss
        //                    objItmPrdType.formatWithFractions = BusinessUtility.GetBool(dtPOPrdItm.Rows[i][28]);//false;  //ipd_frc_fmt
        //                    objItmPrdType.orderedLengthType = BusinessUtility.GetString(dtPOPrdItm.Rows[i][29]); //ipd_ord_lgth_typ
        //                    objItmPrdType.formatDescriptionWithFractions = BusinessUtility.GetBool(dtPOPrdItm.Rows[i][30]); //false; //ipd_fmt_desc_frc
        //                    objItmPrdType.formatDescriptionWithSize = BusinessUtility.GetBool(dtPOPrdItm.Rows[i][31]);//false; //ipd_fmt_size_desc
        //                    objItmPrdType.partCustomerId = BusinessUtility.GetString(dtPOPrdItm.Rows[i][32]);  //ipd_part_cus_id
        //                    objItmPrdType.part = BusinessUtility.GetString(dtPOPrdItm.Rows[i][33]); ; //ipd_part
        //                    objItmPrdType.partNumberRevisionNumber = BusinessUtility.GetString(dtPOPrdItm.Rows[i][34]);  //ipd_part_rev_no
        //                    objItmPrdType.partAccess = BusinessUtility.GetString(dtPOPrdItm.Rows[i][35]); //ipd_part_acs
        //                    objItmPrdType.partControlNumber = BusinessUtility.GetLong(dtPOPrdItm.Rows[i][36]);  //ipd_part_ctl_no
        //                    objItmPrdType.grossWidth = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][37]); //ipd_grs_wdth
        //                    objItmPrdType.grossLength = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][38]); //ipd_grs_lgth
        //                    objItmPrdType.grossPieces = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][39]); //ipd_grs_pcs
        //                    objItmPrdType.grossWidthInteger = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][40]); //ipd_grs_wdth_intgr
        //                    objItmPrdType.grossWidthNumerator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][41]); //ipd_grs_wdth_nmr
        //                    objItmPrdType.grossWidthDenominator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][42]); //ipd_grs_wdth_dnm
        //                    objItmPrdType.grossLengthInteger = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][43]); //ipd_grs_lgth_intgr
        //                    objItmPrdType.grossLengthNumerator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][44]); //ipd_grs_lgth_nmr
        //                    objItmPrdType.grossLengthDenominator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][45]); //ipd_grs_lgth_dnm
        //                    objItmPrdType.widthInteger = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][46]); //ipd_wdth_intgr
        //                    objItmPrdType.widthNumerator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][47]); //ipd_wdth_nmr
        //                    objItmPrdType.widthDenominator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][48]); //ipd_wdth_dnm
        //                    objItmPrdType.lengthInteger = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][49]); //ipd_lgth_intgr
        //                    objItmPrdType.lengthNumerator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][50]); //ipd_lgth_nmr
        //                    objItmPrdType.lengthDenominator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][51]); //ipd_lgth_dnm
        //                    objItmPrdType.innerDiameterInteger = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][52]); //ipd_idia_intgr
        //                    objItmPrdType.innerDiameterNumerator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][53]); //ipd_idia_nmr
        //                    objItmPrdType.innerDiameterDenominator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][54]); //ipd_idia_dnm
        //                    objItmPrdType.outerDiameterInteger = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][55]); //ipd_odia_intgr
        //                    objItmPrdType.outerDiameterNumerator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][56]); //ipd_odia_nmr
        //                    objItmPrdType.outerDiameterDenominator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][57]); //ipd_odia_dnm
        //                    objItmPrdType.customShape = BusinessUtility.GetString(dtPOPrdItm.Rows[i][58]);  //ipd_cstm_sha


        //                    PurchaseOrderItemService.ItemCost objItmCostType = new PurchaseOrderItemService.ItemCost();
        //                    objItmCostType.costNumber = 1; // 1	= Material
        //                    objItmCostType.costDescription = "Material";
        //                    objItmCostType.costNumberSpecified = true;

        //                    objItmCostType.costClass = "I";
        //                    //objItmCostType.cost = 10;
        //                    objItmCostType.costUnitOfMeasure = "LBS";
        //                    //objItmCostType.costQuantity = 496;
        //                    //objItmCostType.costQuantityUnitOfMeasure = "LBS";
        //                    //objItmCostType.exchangeRate = 1;
        //                    // objItmCostType.exchangeRateType = "V";
        //                    //objItmCostType.otherReferenceItemNumber = 0.0M; 
        //                    if (objItmCostType.costClass == "E")
        //                    {
        //                        //objItmCostType.currencyCode = "USD"; //Mandatory when cost class is "E"
        //                        // objItmCostType.vendorId = "1192"; //Mandatory when cost class is "E"
        //                        //objItmCostType.vendorName = "Mukesh"; //Mandatory when cost class is "E"
        //                        //objItmCostType.purchaseOrderPrefix = "PO"; //Mandatory when cost class is "E"
        //                        //objItmCostType.purchaseOrderNumber = 0.0M; //Mandatory when cost class is "E"
        //                        //objItmCostType.purchaseOrderItemNumber = 0.0M; //Mandatory when cost class is "E"
        //                        //objItmCostType.otherReferencePrefix = ""; //Mandatory when cost class is "E"
        //                        //objItmCostType.otherReferenceNumber = 0.0M; //Mandatory when cost class is "E"
        //                        //objItmCostType.vendorReference = ""; //Mandatory when cost class is "E"
        //                    }
        //                    PurchaseOrderItemService.ItemCost[] costsField = new PurchaseOrderItemService.ItemCost[1];
        //                    costsField[0] = objItmCostType;


        //                    objPOItmType = new PurchaseOrderItemService.OrderItem();
        //                    objPOItmType.identity = "";
        //                    objPOItmType.orderPrefix = "PO"; //poi_po_pfx
        //                    objPOItmType.orderNumber = 0;
        //                    //objPOItmType.orderItemNumber = 5858;
        //                    objPOItmType.orderBranchId = "LAX";//"PSM";
        //                    objPOItmType.orderWeightUnitOfMeasure = BusinessUtility.GetString(dtPOItm.Rows[i][0]);//"LBS"; //poi_ord_wgt_um

        //                    //For blanket PO Item
        //                    objPOItmType.blanketPieces = BusinessUtility.GetInt(dtPOItm.Rows[i][1]); //poi_bkt_pcs
        //                    objPOItmType.blanketMeasure = BusinessUtility.GetDecimal(dtPOItm.Rows[i][2]); //poi_bkt_msr
        //                    objPOItmType.blanketWeight = BusinessUtility.GetDecimal(dtPOItm.Rows[i][3]);  //poi_bkt_wgt
        //                    objPOItmType.blanketQuantity = BusinessUtility.GetDecimal(dtPOItm.Rows[i][4]); //poi_bkt_qty
        //                    objPOItmType.blanketWeightUnitOfMeasure = BusinessUtility.GetString(dtPOItm.Rows[i][5]); ; //poi_bkt_wgt_um

        //                    objPOItmType.consignment = BusinessUtility.GetBool(dtPOItm.Rows[i][6]); //false; //poi_consg
        //                    objPOItmType.purchaseCategoryCode = BusinessUtility.GetString(dtPOItm.Rows[i][7]); //"BF"; //poi_pur_cat
        //                    objPOItmType.sourceCode = BusinessUtility.GetString(dtPOItm.Rows[i][8]);  //poi_src
        //                    objPOItmType.allowPartialShipment = BusinessUtility.GetBool(dtPOItm.Rows[i][9]); //true; //poi_alw_prtl_shp
        //                    objPOItmType.overshipPercentage = BusinessUtility.GetDecimal(dtPOItm.Rows[i][10]); //20; //poi_ovrshp_pct
        //                    objPOItmType.undershipPercentage = BusinessUtility.GetDecimal(dtPOItm.Rows[i][11]); //20; //poi_undshp_pct
        //                    objPOItmType.qualityCode = BusinessUtility.GetString(dtPOItm.Rows[i][12]); //"-"; //poi_invt_qlty
        //                    objPOItmType.alloySurchargeApplicationCode = BusinessUtility.GetString(dtPOItm.Rows[i][13]); //"N"; //poi_aly_schg_apln
        //                    objPOItmType.designatedShipToWarehouseId = BusinessUtility.GetString(dtPOItm.Rows[i][14]);  //poi_dsgd_shp_whs
        //                    objPOItmType.vendorPricingSourceCode = BusinessUtility.GetString(dtPOItm.Rows[i][15]); //"M"; //poi_ven_prc_src
        //                    objPOItmType.priceInEffect = BusinessUtility.GetBool(dtPOItm.Rows[i][16]); //false; //poi_prc_in_eff
        //                    objPOItmType.product = objItmPrdType;
        //                    objPOItmType.costs = costsField;

        //                    objDstbType = new PurchaseOrderItemService.Distribution();
        //                    //objDstbType.identity = "";
        //                    //objDstbType.refPrefix = "";
        //                    //objDstbType.refNumber = 0;
        //                    //objDstbType.refItemNumber = 0;
        //                    objDstbType.internalReleaseNumber = BusinessUtility.GetInt(dtPODtl.Rows[i][0]);//1; //New //pod_po_intl_rls_no
        //                    objDstbType.distributionNumber = BusinessUtility.GetInt(dtPODtl.Rows[i][1]);//1; //New  //pod_po_dist
        //                    objDstbType.distributionType = BusinessUtility.GetString(dtPODtl.Rows[i][2]);//"F"; //pod_dist_typ
        //                    objDstbType.shipToWarehouseId = "LAX";
        //                    objDstbType.boughtForCode = BusinessUtility.GetString(dtPODtl.Rows[i][3]);//"C"; //pod_bgt_for
        //                    if (objDstbType.boughtForCode == "C")
        //                    {
        //                        objDstbType.boughtForCustomerId = "L" + BusinessUtility.GetString(dtPODtl.Rows[i][4]);  // "GA6350"; //Mandatory when boughtForCode = "C" //pod_bgt_for_cus_id
        //                        objDstbType.boughtForPart = BusinessUtility.GetString(dtPODtl.Rows[i][5]);  //only set this value when boughtForCode = "C" //pod_bgt_for_part
        //                    }
        //                    else if (objDstbType.boughtForCode == "S")
        //                        objDstbType.boughtForSalesPersonId = BusinessUtility.GetString(dtPODtl.Rows[i][6]);  //Mandatory when boughtForCode = "S" //pod_bgt_for_slp//Be carefull the max length is 4 and 
        //                    objDstbType.orderPrefix = "";
        //                    objDstbType.orderNumber = 0;
        //                    objDstbType.orderItemNumber = 0;
        //                    objDstbType.orderedPieces = BusinessUtility.GetInt(dtPODtl.Rows[i][7]);//101; //pod_ord_pcs
        //                    objDstbType.orderedPiecesType = BusinessUtility.GetString(dtPODtl.Rows[i][8]);//"T"; //pod_ord_pcs_typ
        //                    if (objDstbType.orderedPiecesType == "A")
        //                        objDstbType.orderedPiecesSpecified = true;
        //                    objDstbType.orderedMeasure = BusinessUtility.GetDecimal(dtPODtl.Rows[i][9]);//14603.4313M;//pod_ord_msr
        //                    objDstbType.orderedMeasureType = BusinessUtility.GetString(dtPODtl.Rows[i][10]);//"T"; //pod_ord_msr_typ
        //                    if (objDstbType.orderedMeasureType == "A")
        //                        objDstbType.orderedMeasureSpecified = true;
        //                    objDstbType.orderedWeight = BusinessUtility.GetDecimal(dtPODtl.Rows[i][11]);//3200; //pod_ord_wgt
        //                    objDstbType.orderedWeightType = BusinessUtility.GetString(dtPODtl.Rows[i][12]); //"A"; //pod_ord_wgt_typ
        //                    if (objDstbType.orderedWeightType == "A")
        //                        objDstbType.orderedWeightSpecified = true;
        //                    objDstbType.transitDays = BusinessUtility.GetInt(dtPODtl.Rows[i][13]); //pod_trnst_dy
        //                    objDstbType.freightCost = BusinessUtility.GetDecimal(dtPODtl.Rows[i][14]); //pod_frt_cst
        //                    if (objDstbType.freightCost > 0)
        //                    {
        //                        objDstbType.freightCostUnitOfMeasure = BusinessUtility.GetString(dtPODtl.Rows[i][15]);  // Mandatory if freightCost is entered //pod_frt_cst_um
        //                        objDstbType.freightVendorId = BusinessUtility.GetString(dtPODtl.Rows[i][16]); ; // Mandatory if freightCost is entered //pod_frt_ven_id 
        //                        objDstbType.freightCurrencyCode = BusinessUtility.GetString(dtPODtl.Rows[i][17]); // Mandatory if freightCost is entered //pod_frt_cry
        //                        objDstbType.freightExchangeRate = BusinessUtility.GetDecimal(dtPODtl.Rows[i][18]); // Mandatory if freightCost is entered //pod_frt_exrt 
        //                        objDstbType.freightExchangeRateType = BusinessUtility.GetString(dtPODtl.Rows[i][19]); // Mandatory if freightCost is entered //pod_frt_ex_rt_typ
        //                    }
        //                    objDstbType.inquiryCostUpdated = BusinessUtility.GetString(dtPODtl.Rows[i][21]); //"A"; //pod_inq_upd
        //                    if (objDstbType.inquiryCostUpdated == "M")
        //                        objDstbType.inquiryCost = BusinessUtility.GetDecimal(dtPODtl.Rows[i][20]);//4.02M;// Only set this value when inquiryCostUpdated = "M" //pod_inq_cst

        //                    objDstbType.inquiryCostSpecified = true;
        //                    objDstbType.inquiryRemark = BusinessUtility.GetString(dtPODtl.Rows[i][22]);  //pod_inq_rmk


        //                    //PurchaseOrderItemService.AuthenticationToken oAuthenticationToken = new PurchaseOrderItemService.AuthenticationToken();
        //                    //oAuthenticationToken.username = oCreateWithAutoNumberingResponse.AuthenticationHeader.username;
        //                    //oAuthenticationToken.value = oCreateWithAutoNumberingResponse.AuthenticationHeader.value;

        //                    OriginZones oOriginZones = new OriginZones();

        //                    PurchaseOrderItemService.CreateRequest oCreateRequest =
        //                        new PurchaseOrderItemService.CreateRequest(oAuthenticationToken, oCreateWithAutoNumberingResponse.purchaseOrderIdentity, objPOItmType, objRlsType,
        //                            objDstbType, oOriginZones, objPrsDrcType);

        //                    PurchaseOrderItemService.CreateResponse oCreateResponse = new PurchaseOrderItemService.CreateResponse();

        //                    PurchaseOrderItemService.PurchaseOrderItemService oPurchaseOrderItemServiceClient = new PurchaseOrderItemService.PurchaseOrderItemServiceClient();

        //                    try
        //                    {
        //                        oCreateResponse = oPurchaseOrderItemServiceClient.Create(oCreateRequest);
        //                        oAuthenticationToken = new PurchaseOrderItemService.AuthenticationToken();
        //                        oAuthenticationToken.username = oCreateResponse.AuthenticationHeader.username;
        //                        oAuthenticationToken.value = oCreateResponse.AuthenticationHeader.value;

        //                        objPOHdrAuth = new PurchaseOrderHeaderService.AuthenticationToken();
        //                        objPOHdrAuth.username = oCreateResponse.AuthenticationHeader.username;
        //                        objPOHdrAuth.value = oCreateResponse.AuthenticationHeader.value;

        //                        ErrorLog.createLog("POIteam Token: " + oCreateResponse.AuthenticationHeader.value + "\n POHeader Token User: " + oCreateResponse.AuthenticationHeader.username
        //                     + "\n" + "PO Header Identity:" + oCreateResponse.purchaseOrderItemIdentity);
        //                        //Transaction completed // Ordered item is created
        //                        ErrorLog.createLog("Purchase Order item created successfully PO Item Identity: " + oCreateResponse.purchaseOrderItemIdentity + " Original PO No is " + BusinessUtility.GetString(drPOH[0]));

        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        ErrorLog.createLog("Error during creating POItem,PORls,POtpod and tctipd table for POHeader Iendtity: " + oCreateWithAutoNumberingResponse.purchaseOrderIdentity + "\n" + "Error:" + ex.Message);
        //                        ErrorLog.createLog("Original PO No: " + BusinessUtility.GetString(drPOH[0]) + " Which is not inserted ");
        //                        //PurchaseOrderItemService.CloseRequest objClsRqs = new PurchaseOrderItemService.CloseRequest();
        //                        //PurchaseOrderItemService.CloseResponse objClsRsp = new PurchaseOrderItemService.CloseResponse();

        //                        //oAuthenticationToken = new PurchaseOrderItemService.AuthenticationToken();
        //                        //oAuthenticationToken.username = oCreateWithAutoNumberingResponse.AuthenticationHeader.username;
        //                        //oAuthenticationToken.value = oCreateWithAutoNumberingResponse.AuthenticationHeader.value;
        //                        i = dtPOItm.Rows.Count + 1;
        //                        UserAuthenticationLogOut(iToken);
        //                        GatewayAuthService.AuthenticationToken recurrentToken = new GatewayAuthService.AuthenticationToken();
        //                        recurrentToken = UserAuthentication();

        //                        objPOHdrAuth = new PurchaseOrderHeaderService.AuthenticationToken();
        //                        objPOHdrAuth.username = recurrentToken.username;
        //                        objPOHdrAuth.value = recurrentToken.value;
        //                        iToken = recurrentToken;

        //                    }


        //                    string s = "stop;";

        //                }// end For loop here
        //            }//if condition end here
        //            else
        //            {
        //                ErrorLog.createLog("No. of rows for tables potpoi, potpor, tctipd and potpod are different for PO No:  " + BusinessUtility.GetString(drPOH[0]));
        //                objPOHdrAuth = new PurchaseOrderHeaderService.AuthenticationToken();
        //                objPOHdrAuth.username = oAuthenticationToken.username;
        //                objPOHdrAuth.value = oAuthenticationToken.value;
        //            }
        //        } //Commented foreach PO Header loop end
        //        UserAuthenticationLogOut(iToken);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.createLog("Error during creating POHeader table " + ex.Message);
        //        throw;
        //    }
        //}


        // LATEST ..... USE THIS   <---- 

        public void CreatePurchaseOrderHdrManualNumbering(GatewayAuthService.AuthenticationToken iToken)
        {
           // int iTstPONo = 90064;
            try
            {
                POTransaction objPOTrans = new POTransaction();
                DataTable dtPOH = new DataTable();
                DataTable dtPOItm = new DataTable();
                DataTable dtPORls = new DataTable();
                DataTable dtPODtl = new DataTable();
                DataTable dtPOPrdItm = new DataTable();
                DataTable dtPOFrt = new DataTable();
                DataTable dtPOHdrIns = new DataTable();
                DataTable dtPOItmIns = new DataTable();
                DataTable dtPOCst = new DataTable();
                DataTable dtPOMtlStd = new DataTable();
                DataTable dtPOError = new DataTable();
                try
                {
                    dtPOH = objPOTrans.PurchaseHeaderData();
                }
                catch (Exception ex)
                {
                    ErrorLog.createLog("Error during geting fetching data from potpoh table from database " + ex.Message);
                    throw;
                }


                //Headers objects
                PurchaseOrderHeaderService.PurchaseOrder objPOType;
                PurchaseOrderHeaderService.CreateWithManualNumberingRequest oCreateWithManualNumberingRequest;
                PurchaseOrderHeaderService.CreateWithManualNumberingResponse oCreateWithManualNumberingResponse;
                PurchaseOrderHeaderService.PurchaseOrderService oPurchaseOrderServiceClient;
                PurchaseOrderHeaderService.AuthenticationToken objPOHdrAuth;


                //Gatway Authentication token is same for all PO header
                objPOHdrAuth = new PurchaseOrderHeaderService.AuthenticationToken();
                objPOHdrAuth.username = iToken.username;
                objPOHdrAuth.value = iToken.value;

                // PO Header Processing directory is common for all PO headers
                PurchaseOrderHeaderService.ProcessingDirectives objPrsDrc = new PurchaseOrderHeaderService.ProcessingDirectives();
                objPrsDrc.useDefaultHeaderInformation = true;
                objPrsDrc.useDefaultShipFromNameAndAddress = true;
                objPrsDrc.useDefaultSalesContact = true;
                
                objPrsDrc.useDefaultAuthorizationAndTransactionStatus = true;



                // PO Items Objects
                PurchaseOrderItemService.Release objRlsType;
                PurchaseOrderItemService.ItemProduct objItmPrdType;
                PurchaseOrderItemService.OrderItem objPOItmType;
                PurchaseOrderItemService.Distribution objDstbType;
                PurchaseOrderItemService.AuthenticationToken oAuthenticationToken;


                //PO Items Processing directory is common for all PO Items
                PurchaseOrderItemService.ProcessingDirectives objPrsDrcType = new PurchaseOrderItemService.ProcessingDirectives();
                objPrsDrcType.useDefaultDetailInformation = true;
                
                objPrsDrcType.useDefaultTransactionStatus = true;
                objPrsDrcType.useDefaultProductDescriptions = true;
                objPrsDrcType.useDefaultConditions = true;
                objPrsDrcType.useDefaultTolerances = true;
                objPrsDrcType.useDefaultCoilPackagings = true;
                objPrsDrcType.useDefaultPackaging = true;
                objPrsDrcType.useDefaultTests = true;
                objPrsDrcType.useDefaultChemicalSpecifications = true;
                objPrsDrcType.useDefaultTestCertificates = true;
                objPrsDrcType.useDefaultOriginZones = true;

                foreach (DataRow drPOH in dtPOH.Rows)
                {
                    //Start creating purchase order header table from table potpoh 

                    objPOType = new PurchaseOrderHeaderService.PurchaseOrder();
                    objPOType.identity = "";
                    objPOType.orderPrefix = "PO";
                    objPOType.orderNumber = 0;      // 
                    objPOType.orderBranchId = "LAX";//BusinessUtility.GetString(drPOH[1]);
                    if (BusinessUtility.GetString(drPOH[2]) != "") 
                    {
                        objPOType.vendorId = "L" + BusinessUtility.GetString(drPOH[2]);//"98"; //"L-1192";//poh_ven_id // Prefix "L-" is medatory from table aprven_rec where ven_ven_id like  '%1210'
                    }
                    objPOType.shipFromId = BusinessUtility.GetInt(drPOH[3]); //poh_shp_fm
                    objPOType.millId = BusinessUtility.GetString(drPOH[4]); //poh_mill
                    objPOType.orderType = BusinessUtility.GetString(drPOH[5]); //poh_po_typ
                    objPOType.invoiceMailToBranchId = "LAX"; //BusinessUtility.GetString(drPOH[6]);
                    objPOType.paymentTermCode = BusinessUtility.GetInt(drPOH[7]); //poh_pur_pttrm
                    //objPOType.paymentTermCodeSpecified =true;
                    objPOType.discountTermCode = BusinessUtility.GetInt(drPOH[8]); //poh_pur_disc_trm
                    //objPOType.discountTermCodeSpecified = true;
                    objPOType.buyerId = BusinessUtility.GetString(drPOH[9]);//"JE"; //poh_byr
                    objPOType.currencyCode = BusinessUtility.GetString(drPOH[10]);//"USD"; //poh_cry
                    objPOType.exchangeRate = BusinessUtility.GetDecimal(drPOH[11]); //poh_exrt
                    objPOType.exchangeRateType = BusinessUtility.GetString(drPOH[12]);//"V";  //poh_ex_rt_typ
                    objPOType.shipItemsTogether = BusinessUtility.GetBool(drPOH[13]); //poh_shp_itm_tgth
                    //objPOType.shipItemsTogetherSpecified = true;
                    objPOType.acknowledgementRequired = BusinessUtility.GetBool(drPOH[14]); //poh_ack_reqd
                    //objPOType.acknowledgementRequiredSpecified = true;
                    objPOType.orderPlacedDate = BusinessUtility.GetDateTime(drPOH[15]);//BusinessUtility.GetDateTime("2015/03/23", DateFormat.yyyyMMdd);   // poh_po_pl_dt //New
                    //objPOType.orderPlacedDateSpecified = true;
                    //objPOType.purchaseOrderIdentity = "";


                    // Update Freight if FRT-FRT Exist
                    // NOTE GET ALL values for given PO - PFX, NO, and ITEM = 0 from frtfrt_rec
                    try
                    {
                        dtPOFrt = objPOTrans.PurchaseOrderFreight(BusinessUtility.GetString(drPOH[0]));
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.createLog("Error during geting fetching data from frtfrt table from database " + ex.Message);
                        throw;
                    }

                    if (dtPOFrt.Rows.Count > 0)
                    {
                        objPrsDrc.useDefaultFreight = false;
                    }
                    else
                    {
                        objPrsDrc.useDefaultFreight = true;
                    }

                    PurchaseOrderHeaderService.Freight poFrt = new PurchaseOrderHeaderService.Freight();
                    if (dtPOFrt.Rows.Count > 0)
                    {
                        poFrt.deliveryMethodCode = BusinessUtility.GetString(dtPOFrt.Rows[0][0]);//"CC";
                        poFrt.exchangeRateSpecified = true;
                        poFrt.tradeTermCode = BusinessUtility.GetString(dtPOFrt.Rows[0][1]);//"EXW";
                        
                        poFrt.carrierName = BusinessUtility.GetString(dtPOFrt.Rows[0][2]);//"iTECH Frieght Vendor";
                        poFrt.cost = BusinessUtility.GetDecimal(dtPOFrt.Rows[0][3]);//0.1800M;
                        poFrt.costSpecified = true;
                        poFrt.costUnitOfMeasure = BusinessUtility.GetString(dtPOFrt.Rows[0][4]);//"LBS";  // Mandatory if freightCost is entered //pod_frt_cst_um
                        if (BusinessUtility.GetString(dtPOFrt.Rows[0][5]) != "")
                        {
                            poFrt.freightVendorId = "L" + BusinessUtility.GetString(dtPOFrt.Rows[0][5]);//"28"; // Mandatory if freightCost is entered //pod_frt_ven_id 
                        }
                        poFrt.currencyCode = BusinessUtility.GetString(dtPOFrt.Rows[0][6]);//"USD"; // Mandatory if freightCost is entered //pod_frt_cry
                        // poFrt.freightCost = 0.1800M;
                        poFrt.exchangeRate = BusinessUtility.GetDecimal(dtPOFrt.Rows[0][7]);//1;
                        poFrt.exchangeRateType = BusinessUtility.GetString(dtPOFrt.Rows[0][8]); //"V";
                        objPOType.freight = poFrt;
                    }

                    //Header Instruction tsi table 
                    try
                    {
                        dtPOHdrIns = objPOTrans.PurchaseOrderHdrInstruction(BusinessUtility.GetString(drPOH[0]));
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.createLog("Error during geting fetching data from tcttsi_rec table from database " + ex.Message);
                        throw;
                    }

                    if (dtPOHdrIns.Rows.Count > 0)
                    {
                        objPrsDrc.useDefaultInstructions = false;
                    }
                    else
                    {
                        objPrsDrc.useDefaultInstructions = true;
                    }

                    int iNoOfHdrInstruction = dtPOHdrIns.Rows.Count;
                    PurchaseOrderHeaderService.Instruction[] hdrInstruction = new PurchaseOrderHeaderService.Instruction[iNoOfHdrInstruction];
                    // NOTE GET ALL values for given PO - PFX, NO

                    //foreach (DataRow drCTTCSI in dtCTTCSI.Rows)  
                    for (int iRow = 0; iRow < iNoOfHdrInstruction; iRow++)
                    {
                        PurchaseOrderHeaderService.Instruction objHdrIns = new PurchaseOrderHeaderService.Instruction();

                        objHdrIns.remarkType = BusinessUtility.GetString(dtPOHdrIns.Rows[iRow][5]);
                        objHdrIns.languageCode = BusinessUtility.GetString(dtPOHdrIns.Rows[iRow][6]);
                        objHdrIns.libraryId = BusinessUtility.GetString(dtPOHdrIns.Rows[iRow][7]);
                        objHdrIns.text = BusinessUtility.GetString(dtPOHdrIns.Rows[iRow][8]);
                        hdrInstruction[iRow] = objHdrIns;
                    }
                    if (dtPOHdrIns.Rows.Count > 0)
                    {
                        objPOType.instructions = hdrInstruction;
                    }

                     oCreateWithManualNumberingRequest = new PurchaseOrderHeaderService.CreateWithManualNumberingRequest(objPOHdrAuth, objPOType, objPrsDrc, BusinessUtility.GetInt(drPOH[0]));
                    //iTstPONo++;
                    //oCreateWithManualNumberingRequest = new PurchaseOrderHeaderService.CreateWithManualNumberingRequest(objPOHdrAuth, objPOType, objPrsDrc, iTstPONo);

                    oCreateWithManualNumberingResponse = new PurchaseOrderHeaderService.CreateWithManualNumberingResponse();

                    oPurchaseOrderServiceClient = new PurchaseOrderHeaderService.PurchaseOrderServiceClient();

                    // PO Header Identtity is generate
                    try
                    {
                        oCreateWithManualNumberingResponse = oPurchaseOrderServiceClient.CreateWithManualNumbering(oCreateWithManualNumberingRequest);


                        //ErrorLog.createLog("POHeader Token: " + oCreateWithAutoNumberingResponse.AuthenticationHeader.value + "\n POHeader Token User: " + oCreateWithAutoNumberingResponse.AuthenticationHeader.username
                        //    + "\n" + "PO Header Identity:" + oCreateWithAutoNumberingResponse.purchaseOrderIdentity);

                        oAuthenticationToken = new PurchaseOrderItemService.AuthenticationToken();
                        oAuthenticationToken.username = oCreateWithManualNumberingResponse.AuthenticationHeader.username;
                        oAuthenticationToken.value = oCreateWithManualNumberingResponse.AuthenticationHeader.value;

                        //Start creating Purchase order items 

                        // Get data from PO Item table potpoi
                        try
                        {
                            dtPOItm = objPOTrans.PurchaseOrderItemData(BusinessUtility.GetString(drPOH[0]));
                        }
                        catch (Exception ex)
                        {
                            ErrorLog.createLog("Error during geting fetching data from potpoi table from database for PO no. " + BusinessUtility.GetString(drPOH[0]) +
                                "\n" + ex.Message);
                            // throw;
                        }



                        // Get data from PO Item table potpor
                        try
                        {
                            dtPORls = objPOTrans.PurchaseOrderRlsData(BusinessUtility.GetString(drPOH[0]));
                        }
                        catch (Exception ex)
                        {
                            ErrorLog.createLog("Error during geting fetching data from potpor table from database for PO no. " + BusinessUtility.GetString(drPOH[0]) +
                                "\n" + ex.Message);
                            // throw;
                        }


                        // Get data from PO Item table potpod
                        try
                        {
                            dtPODtl = objPOTrans.PurchaseOrderDistData(BusinessUtility.GetString(drPOH[0]));
                        }
                        catch (Exception ex)
                        {
                            ErrorLog.createLog("Error during geting fetching data from potpod table from database for PO no. " + BusinessUtility.GetString(drPOH[0]) +
                                "\n" + ex.Message);
                            // throw;
                        }

                        // Get data from PO Item table tctipd
                        try
                        {
                            dtPOPrdItm = objPOTrans.PurchaseOrderPrdItm(BusinessUtility.GetString(drPOH[0]));
                        }
                        catch (Exception ex)
                        {
                            ErrorLog.createLog("Error during geting fetching data from tctipd table from database for PO no. " + BusinessUtility.GetString(drPOH[0]) +
                                "\n" + ex.Message);
                            // throw;
                        }




                        //oPurchaseOrderServiceClient.UpdateFreight(poFrt);



                        //        new PurchaseOrderItemService.CreateRequest(oAuthenticationToken, oCreateWithManualNumberingResponse.purchaseOrderIdentity, objPOItmType, objRlsType,
                        //            objDstbType, oOriginZones, objPrsDrcType);

                        //    PurchaseOrderItemService.CreateResponse oCreateResponse = new PurchaseOrderItemService.CreateResponse();

                        //    PurchaseOrderItemService.PurchaseOrderItemService oPurchaseOrderItemServiceClient = new PurchaseOrderItemService.PurchaseOrderItemServiceClient();

                        //    try
                        //    {
                        //        oCreateResponse = oPurchaseOrderItemServiceClient.Create(oCreateRequest);

                        //check oCreateWithManualNumberingResponse.purchaseOrderIdentity value is not null and empty


                        if (dtPOItm.Rows.Count == dtPORls.Rows.Count && dtPOItm.Rows.Count == dtPODtl.Rows.Count && dtPODtl.Rows.Count == dtPOPrdItm.Rows.Count)
                        {
                            for (int i = 0; i < dtPOItm.Rows.Count; i++)
                            {
                                // Set PO Release values


                                objRlsType = new PurchaseOrderItemService.Release();
                                //objRlsType.identity = "";
                                //objRlsType.refPrefix = "PO"; //por_po_pfx //New
                                //objRlsType.refNumber = 222;
                                //objRlsType.refItemNumber = 586;

                                objRlsType.internalReleaseNumber = BusinessUtility.GetInt(dtPORls.Rows[i][0]);//1; //por_po_intl_rls_no //
                                objRlsType.releaseNumber = BusinessUtility.GetString(dtPORls.Rows[i][1]); //por_po_rls_no //
                                objRlsType.readyPieces = BusinessUtility.GetInt(dtPORls.Rows[i][2]); //por_rdy_pcs
                                objRlsType.readyMeasure = BusinessUtility.GetDecimal(dtPORls.Rows[i][3]); //por_rdy_msr
                                objRlsType.readyWeight = BusinessUtility.GetDecimal(dtPORls.Rows[i][4]); //por_rdy_wgt
                                objRlsType.readyQuantity = BusinessUtility.GetInt(dtPORls.Rows[i][5]); //por_rdy_qty
                                objRlsType.readyDate = BusinessUtility.GetDateTime(dtPORls.Rows[i][6]); //por_rdy_dt
                                objRlsType.defaultDueDateQualifier = BusinessUtility.GetString(dtPORls.Rows[i][7]); //por_due_dt_qlf
                                // its showing error in table sctmsg "Due Date is invalid" when defaultDueDateFormat = "D" For now we are passing  "T" values
                                if (BusinessUtility.GetDateTime(dtPORls.Rows[i][10]) >= DateTime.Now )
                                { 
                                 objRlsType.defaultDueDateFormat = BusinessUtility.GetString(dtPORls.Rows[i][8]);//"T"; //por_due_dt_fmt 
                                }
                                else
                                { 
                                    objRlsType.defaultDueDateFormat = "T";
                                }

                                if (objRlsType.defaultDueDateFormat != "T")
                                {
                                    objRlsType.dueDateEntry = BusinessUtility.GetInt(dtPORls.Rows[i][9]);//Blank when defaultDueDateFormat = "T"; // por_due_dt_ent  //YYYYMMDD 
                                    objRlsType.dueDateEntrySpecified = true;
                                }


                                objRlsType.dueFromDate = BusinessUtility.GetDateTime(dtPORls.Rows[i][10]); //por_due_fm_dt //New //10/15/2014 //Convert.ToDateTime("2015-12-18"); Convert.ToDateTime(Convert.ToDateTime(dtPORls.Rows[i][10]).ToString("yyyy-MM-dd"));  //
                                objRlsType.dueToDate = BusinessUtility.GetDateTime(dtPORls.Rows[i][11]); //por_due_to_dt //Convert.ToDateTime("2015-12-18"); Convert.ToDateTime(Convert.ToDateTime(dtPORls.Rows[i][11]).ToString("yyyy-MM-dd"));  // 
                                objRlsType.dueDateWeekNo = BusinessUtility.GetInt(dtPORls.Rows[i][12]); //por_due_dt_wk_no //New
                                objRlsType.dueDateYear = BusinessUtility.GetInt(dtPORls.Rows[i][13]); //por_due_dt_wk_cy //New
                                objRlsType.rollingSchedule = BusinessUtility.GetString(dtPORls.Rows[i][14]); //por_roll_sch
                                objRlsType.acknowledgementReceived = BusinessUtility.GetBool(dtPORls.Rows[i][15]);//false;
                                if (objRlsType.acknowledgementReceived == true)
                                    objRlsType.acknowledgementDate = BusinessUtility.GetDateTime(dtPORls.Rows[i][16]); // Must set value when objRlsType.acknowledgementReceived = true //por_ack_dt
                                //objRlsType.millOrderNumber = "";


                                objItmPrdType = new PurchaseOrderItemService.ItemProduct();
                                objItmPrdType.formId = BusinessUtility.GetString(dtPOPrdItm.Rows[i][0]); //"SSRD"; //ipd_frm
                                objItmPrdType.gradeId = BusinessUtility.GetString(dtPOPrdItm.Rows[i][1]);//"13-8"; //ipd_grd
                                objItmPrdType.numericSize1 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][2]); //1; //ipd_num_size1
                                objItmPrdType.numericSize1Specified = true;
                                objItmPrdType.numericSize2 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][3]);
                                objItmPrdType.numericSize2Specified = true;
                                objItmPrdType.numericSize3 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][4]);
                                objItmPrdType.numericSize3Specified = true;
                                objItmPrdType.numericSize4 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][5]);
                                objItmPrdType.numericSize4Specified = true;
                                objItmPrdType.numericSize5 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][6]);
                                objItmPrdType.numericSize5Specified = true;
                                //if (BusinessUtility.GetDouble(dtPOPrdItm.Rows[i][7]) == 1)
                                //{
                                //    objItmPrdType.sizeId = "1.00";
                                //}
                                //else if (BusinessUtility.GetDouble(dtPOPrdItm.Rows[i][7]) == 2.75)
                                //{
                                //    objItmPrdType.sizeId = "2.875";
                                //}
                                //else
                                //{
                                //    objItmPrdType.sizeId = BusinessUtility.GetString(dtPOPrdItm.Rows[i][7]); //"1"; //ipd_size
                                //}
                                objItmPrdType.sizeId = BusinessUtility.GetString(dtPOPrdItm.Rows[i][7]); //"1"; //ipd_size
                                
                                objItmPrdType.finishId = BusinessUtility.GetString(dtPOPrdItm.Rows[i][8]);

                                //string[] stExtendFns = new string[] { "US", "2.5" };
                                //objItmPrdType.extendedFinish = stExtendFns;
                                objItmPrdType.width = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][9]); //ipd_wdth
                                objItmPrdType.widthSpecified = true;
                                objItmPrdType.length = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][10]); //144; //ipd_lgth
                                objItmPrdType.lengthSpecified = true;
                                objItmPrdType.dimensionDesignator = BusinessUtility.GetString(dtPOPrdItm.Rows[i][11]); //"F"; //ipd_dim_dsgn
                                objItmPrdType.innerDiameter = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][12]); //ipd_idia
                                objItmPrdType.outerDiameter = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][13]);  //ipd_odia
                                //objItmPrdType.wallThickness = 0.0M;
                                objItmPrdType.gaugeSize = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][14]); //ipd_ga_size
                                objItmPrdType.gaugeType = BusinessUtility.GetString(dtPOPrdItm.Rows[i][15]);//"N"; //ipd_ga_typ
                                objItmPrdType.alternateSize = BusinessUtility.GetString(dtPOPrdItm.Rows[i][16]);  //ipd_alt_size
                                objItmPrdType.randomDimension1 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][17]); //ipd_rdm_dim_1
                                objItmPrdType.randomDimension2 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][18]); //ipd_rdm_dim_2
                                objItmPrdType.randomDimension3 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][19]); //ipd_rdm_dim_3
                                objItmPrdType.randomDimension4 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][20]);//ipd_rdm_dim_4
                                objItmPrdType.randomDimension5 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][21]); //ipd_rdm_dim_5
                                objItmPrdType.randomDimension6 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][22]); //ipd_rdm_dim_6
                                objItmPrdType.randomDimension7 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][23]); //ipd_rdm_dim_7
                                objItmPrdType.randomDimension8 = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][24]);//ipd_rdm_dim_8
                                if (objItmPrdType.dimensionDesignator != "M" && objItmPrdType.dimensionDesignator != "F")
                                {
                                    objItmPrdType.randomArea = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][25]);//Ignored if dimensionDesignator = "M" or "F" //ipd_rdm_area
                                }
                                objItmPrdType.customerVendorId = "L" + BusinessUtility.GetString(dtPOPrdItm.Rows[i][26]); //"L-1039"; //ipd_cus_ven_id
                                objItmPrdType.entryMeasure = BusinessUtility.GetString(dtPOPrdItm.Rows[i][27]);//"E"; //ipd_ent_msr
                                objItmPrdType.lengthInFeetOrMeters = false; //need to discuss
                                objItmPrdType.formatWithFractions = BusinessUtility.GetBool(dtPOPrdItm.Rows[i][28]);//false;  //ipd_frc_fmt
                                objItmPrdType.orderedLengthType = BusinessUtility.GetString(dtPOPrdItm.Rows[i][29]); //ipd_ord_lgth_typ
                                objItmPrdType.formatDescriptionWithFractions = BusinessUtility.GetBool(dtPOPrdItm.Rows[i][30]); //false; //ipd_fmt_desc_frc
                                objItmPrdType.formatDescriptionWithSize = BusinessUtility.GetBool(dtPOPrdItm.Rows[i][31]);//false; //ipd_fmt_size_desc
                                objItmPrdType.partCustomerId = BusinessUtility.GetString(dtPOPrdItm.Rows[i][32]);  //ipd_part_cus_id
                                objItmPrdType.part = BusinessUtility.GetString(dtPOPrdItm.Rows[i][33]); ; //ipd_part
                                objItmPrdType.partNumberRevisionNumber = BusinessUtility.GetString(dtPOPrdItm.Rows[i][34]);  //ipd_part_rev_no
                                objItmPrdType.partAccess = BusinessUtility.GetString(dtPOPrdItm.Rows[i][35]); //ipd_part_acs
                                objItmPrdType.partControlNumber = BusinessUtility.GetLong(dtPOPrdItm.Rows[i][36]);  //ipd_part_ctl_no
                                objItmPrdType.grossWidth = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][37]); //ipd_grs_wdth
                                objItmPrdType.grossLength = BusinessUtility.GetDecimal(dtPOPrdItm.Rows[i][38]); //ipd_grs_lgth
                                objItmPrdType.grossPieces = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][39]); //ipd_grs_pcs
                                objItmPrdType.grossWidthInteger = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][40]); //ipd_grs_wdth_intgr
                                objItmPrdType.grossWidthNumerator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][41]); //ipd_grs_wdth_nmr
                                objItmPrdType.grossWidthDenominator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][42]); //ipd_grs_wdth_dnm
                                objItmPrdType.grossLengthInteger = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][43]); //ipd_grs_lgth_intgr
                                objItmPrdType.grossLengthNumerator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][44]); //ipd_grs_lgth_nmr
                                objItmPrdType.grossLengthDenominator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][45]); //ipd_grs_lgth_dnm
                                objItmPrdType.widthInteger = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][46]); //ipd_wdth_intgr
                                objItmPrdType.widthNumerator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][47]); //ipd_wdth_nmr
                                objItmPrdType.widthDenominator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][48]); //ipd_wdth_dnm
                                objItmPrdType.lengthInteger = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][49]); //ipd_lgth_intgr
                                objItmPrdType.lengthNumerator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][50]); //ipd_lgth_nmr
                                objItmPrdType.lengthDenominator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][51]); //ipd_lgth_dnm
                                objItmPrdType.innerDiameterInteger = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][52]); //ipd_idia_intgr
                                objItmPrdType.innerDiameterNumerator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][53]); //ipd_idia_nmr
                                objItmPrdType.innerDiameterDenominator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][54]); //ipd_idia_dnm
                                objItmPrdType.outerDiameterInteger = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][55]); //ipd_odia_intgr
                                objItmPrdType.outerDiameterNumerator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][56]); //ipd_odia_nmr
                                objItmPrdType.outerDiameterDenominator = BusinessUtility.GetInt(dtPOPrdItm.Rows[i][57]); //ipd_odia_dnm
                                objItmPrdType.customShape = BusinessUtility.GetString(dtPOPrdItm.Rows[i][58]);  //ipd_cstm_sha

                                //PurchaseOrderItmCost
                                //fetch data cttcsi table 
                                try
                                {
                                    dtPOCst = objPOTrans.PurchaseOrderItmCost(BusinessUtility.GetString(drPOH[0]), BusinessUtility.GetString(dtPOItm.Rows[i][17]));
                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.createLog("Error during geting fetching data from cttcsi table from database for PO no. " + BusinessUtility.GetString(drPOH[0]) +
                                        "\n" + ex.Message);
                                    // throw;
                                }

                                int iNoOfCostRecords = dtPOCst.Rows.Count;
                                PurchaseOrderItemService.ItemCost[] costsField = new PurchaseOrderItemService.ItemCost[iNoOfCostRecords];  // Based on number of rows returned by CTTCSI_REC  csi_cc_typ = 1
                                // NOTE GET ALL values for given PO - PFX, NO, and ITM from cttcsi_rec where csi_cc_typ = 1 and LOOP


                                //foreach (DataRow drCTTCSI in dtCTTCSI.Rows)  
                                for (int iRow = 0; iRow < iNoOfCostRecords; iRow++)
                                {
                                    PurchaseOrderItemService.ItemCost objItmCostType = new PurchaseOrderItemService.ItemCost();
                                    objItmCostType.costNumber = BusinessUtility.GetInt(dtPOCst.Rows[iRow][0]);//1; // 1	= Material
                                    objItmCostType.costDescription = BusinessUtility.GetString(dtPOCst.Rows[iRow][1]);//"Material";
                                    objItmCostType.costNumberSpecified = true;
                                    objItmCostType.costClass = BusinessUtility.GetString(dtPOCst.Rows[iRow][2]);// "V";
                                    objItmCostType.cost = BusinessUtility.GetDecimal(dtPOCst.Rows[iRow][3]);//7.6700M;
                                    objItmCostType.costUnitOfMeasure = BusinessUtility.GetString(dtPOCst.Rows[iRow][4]); //"LBS";
                                    objItmCostType.costSpecified = true;
                                    costsField[iRow] = objItmCostType;
                                }



                                // NEW Mat Standard 
                                //PurchaseOrderMaterial Standard
                                //fetch data mcrmss_rec table 
                                try
                                {
                                    dtPOMtlStd = objPOTrans.PurchaseOrderMtlStd(BusinessUtility.GetString(drPOH[0]), BusinessUtility.GetString(dtPOItm.Rows[i][17]));
                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.createLog("Error during geting fetching data from mcrmss_rec table from database for PO no. " + BusinessUtility.GetString(drPOH[0]) +
                                        "\n" + ex.Message);
                                    // throw;
                                }

                                if (dtPOMtlStd.Rows.Count > 0)
                                {
                                    objPrsDrcType.useDefaultStandardSpecs = false;  // <-- true if select mcrmss_rec.* from pntssp_rec  join mcrmss_rec on mss_mss_ctl_no = ssp_mss_ctl_no where ssp_cmpy_id = 'PS' and  ssp_ref_pfx = 'PO' and ssp_ref_no = 91548 and ssp_ref_itm = <item no> does not return any record
                                }
                                else
                                {
                                    objPrsDrcType.useDefaultStandardSpecs = true;  // <-- true if select mcrmss_rec.* from pntssp_rec  join mcrmss_rec on mss_mss_ctl_no = ssp_mss_ctl_no where ssp_cmpy_id = 'PS' and  ssp_ref_pfx = 'PO' and ssp_ref_no = 91548 and ssp_ref_itm = <item no> does not return any record
                                }

                                int iNoOfSpecRecords = dtPOMtlStd.Rows.Count;
                                PurchaseOrderItemService.StandardSpecification[] specField = new PurchaseOrderItemService.StandardSpecification[iNoOfSpecRecords];  // Based on number of rows returned by CTTCSI_REC  csi_cc_typ = 1
                                // NOTE GET ALL values for given PO - PFX, NO, and ITM from cttcsi_rec where csi_cc_typ = 1 and LOOP

                                //foreach (DataRow drCTTCSI in dtCTTCSI.Rows)  
                                for (int iRow = 0; iRow < iNoOfSpecRecords; iRow++)
                                {
                                    PurchaseOrderItemService.StandardSpecification objItmSpecType = new PurchaseOrderItemService.StandardSpecification();
                                    objItmSpecType.standardDevelopmentOrganization = BusinessUtility.GetString(dtPOMtlStd.Rows[iRow][0]); // "AMS";
                                    objItmSpecType.standardId = BusinessUtility.GetString(dtPOMtlStd.Rows[iRow][1]); //"5732";
                                    objItmSpecType.additionalId = BusinessUtility.GetString(dtPOMtlStd.Rows[iRow][2]); //"Rev. J";
                                    specField[iRow] = objItmSpecType;
                                }

                                //Item Instruction tsi table 
                                try
                                {
                                    dtPOItmIns = objPOTrans.PurchaseOrderItmInstruction(BusinessUtility.GetString(drPOH[0]), BusinessUtility.GetString(dtPOItm.Rows[i][17]));
                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.createLog("Error during geting fetching data from tcttsi_rec table from database " + ex.Message);
                                    throw;
                                }

                                if (dtPOItmIns.Rows.Count > 0)
                                {
                                    objPrsDrcType.useDefaultInstructions =  false;
                                }
                                else
                                {
                                    objPrsDrcType.useDefaultInstructions = true;
                                }

                                int iNoOfItmInstruction = dtPOItmIns.Rows.Count;
                                PurchaseOrderItemService.Instruction[] itmInstruction = new PurchaseOrderItemService.Instruction[iNoOfItmInstruction];
                                // NOTE GET ALL values for given PO - PFX, NO

                                //foreach (DataRow drCTTCSI in dtCTTCSI.Rows)  
                                for (int iRow = 0; iRow < iNoOfItmInstruction; iRow++)
                                {
                                    PurchaseOrderItemService.Instruction objItmIns = new PurchaseOrderItemService.Instruction();

                                    objItmIns.remarkType = BusinessUtility.GetString(dtPOItmIns.Rows[iRow][5]);
                                    objItmIns.languageCode = BusinessUtility.GetString(dtPOItmIns.Rows[iRow][6]);
                                    objItmIns.libraryId = BusinessUtility.GetString(dtPOItmIns.Rows[iRow][7]);
                                    objItmIns.text = BusinessUtility.GetString(dtPOItmIns.Rows[iRow][8]);
                                    itmInstruction[iRow] = objItmIns;
                                }
                                


                                objPOItmType = new PurchaseOrderItemService.OrderItem();
                                objPOItmType.identity = "";
                                objPOItmType.orderPrefix = "PO"; //poi_po_pfx
                                objPOItmType.orderNumber = 0;
                                //objPOItmType.orderItemNumber = 5858;
                                objPOItmType.orderBranchId = "LAX";//"PSM";
                                objPOItmType.orderWeightUnitOfMeasure = BusinessUtility.GetString(dtPOItm.Rows[i][0]);//"LBS"; //poi_ord_wgt_um

                                //For blanket PO Item
                                objPOItmType.blanketPieces = BusinessUtility.GetInt(dtPOItm.Rows[i][1]); //poi_bkt_pcs
                                objPOItmType.blanketMeasure = BusinessUtility.GetDecimal(dtPOItm.Rows[i][2]); //poi_bkt_msr
                                objPOItmType.blanketWeight = BusinessUtility.GetDecimal(dtPOItm.Rows[i][3]);  //poi_bkt_wgt
                                objPOItmType.blanketQuantity = BusinessUtility.GetDecimal(dtPOItm.Rows[i][4]); //poi_bkt_qty
                                objPOItmType.blanketWeightUnitOfMeasure = BusinessUtility.GetString(dtPOItm.Rows[i][5]); ; //poi_bkt_wgt_um

                                objPOItmType.consignment = BusinessUtility.GetBool(dtPOItm.Rows[i][6]); //false; //poi_consg
                                objPOItmType.purchaseCategoryCode = BusinessUtility.GetString(dtPOItm.Rows[i][7]); //"BF"; //poi_pur_cat
                                objPOItmType.sourceCode = BusinessUtility.GetString(dtPOItm.Rows[i][8]);  //poi_src
                                objPOItmType.allowPartialShipment = BusinessUtility.GetBool(dtPOItm.Rows[i][9]); //true; //poi_alw_prtl_shp
                                objPOItmType.overshipPercentage = BusinessUtility.GetDecimal(dtPOItm.Rows[i][10]); //20; //poi_ovrshp_pct
                                objPOItmType.undershipPercentage = BusinessUtility.GetDecimal(dtPOItm.Rows[i][11]); //20; //poi_undshp_pct
                                objPOItmType.qualityCode = BusinessUtility.GetString(dtPOItm.Rows[i][12]); //"-"; //poi_invt_qlty
                                objPOItmType.alloySurchargeApplicationCode = BusinessUtility.GetString(dtPOItm.Rows[i][13]); //"N"; //poi_aly_schg_apln
                                objPOItmType.designatedShipToWarehouseId = BusinessUtility.GetString(dtPOItm.Rows[i][14]);  //poi_dsgd_shp_whs
                                objPOItmType.vendorPricingSourceCode = BusinessUtility.GetString(dtPOItm.Rows[i][15]); //"M"; //poi_ven_prc_src
                                objPOItmType.priceInEffect = BusinessUtility.GetBool(dtPOItm.Rows[i][16]); //false; //poi_prc_in_eff
                                objPOItmType.product = objItmPrdType;
                                objPOItmType.costs = costsField;
                                if (dtPOMtlStd.Rows.Count > 0)
                                {
                                    objPOItmType.standardSpecs = specField; // <-- Adding Specs to the item
                                }
                                if (dtPOItmIns.Rows.Count > 0)
                                {
                                    objPOItmType.instructions = itmInstruction;
                                }

                                objDstbType = new PurchaseOrderItemService.Distribution();
                                //objDstbType.identity = "";
                                //objDstbType.refPrefix = "";
                                //objDstbType.refNumber = 0;
                                //objDstbType.refItemNumber = 0;
                                objDstbType.internalReleaseNumber = BusinessUtility.GetInt(dtPODtl.Rows[i][0]);//1; //New //pod_po_intl_rls_no
                                objDstbType.distributionNumber = BusinessUtility.GetInt(dtPODtl.Rows[i][1]);//1; //New  //pod_po_dist
                                objDstbType.distributionType = BusinessUtility.GetString(dtPODtl.Rows[i][2]);//"F"; //pod_dist_typ
                                objDstbType.shipToWarehouseId = "LAX";
                                objDstbType.boughtForCode = BusinessUtility.GetString(dtPODtl.Rows[i][3]);//"C"; //pod_bgt_for
                                if (objDstbType.boughtForCode == "C")
                                {
                                    objDstbType.boughtForCustomerId = "L" + BusinessUtility.GetString(dtPODtl.Rows[i][4]);  // "GA6350"; //Mandatory when boughtForCode = "C" //pod_bgt_for_cus_id
                                    objDstbType.boughtForPart = BusinessUtility.GetString(dtPODtl.Rows[i][5]);  //only set this value when boughtForCode = "C" //pod_bgt_for_part
                                }
                                else if (objDstbType.boughtForCode == "S")
                                    objDstbType.boughtForSalesPersonId = BusinessUtility.GetString(dtPODtl.Rows[i][6]);  //Mandatory when boughtForCode = "S" //pod_bgt_for_slp//Be carefull the max length is 4 and 
                                objDstbType.orderPrefix = "";
                                objDstbType.orderNumber = 0;
                                objDstbType.orderItemNumber = 0;
                                objDstbType.orderedPieces = BusinessUtility.GetInt(dtPODtl.Rows[i][7]);//101; //pod_ord_pcs
                                objDstbType.orderedPiecesType = BusinessUtility.GetString(dtPODtl.Rows[i][8]);//"T"; //pod_ord_pcs_typ
                                if (objDstbType.orderedPiecesType == "A")
                                    objDstbType.orderedPiecesSpecified = true;
                                objDstbType.orderedMeasure = BusinessUtility.GetDecimal(dtPODtl.Rows[i][9]);//14603.4313M;//pod_ord_msr
                                objDstbType.orderedMeasureType = BusinessUtility.GetString(dtPODtl.Rows[i][10]);//"T"; //pod_ord_msr_typ
                                if (objDstbType.orderedMeasureType == "A")
                                    objDstbType.orderedMeasureSpecified = true;
                                objDstbType.orderedWeight = BusinessUtility.GetDecimal(dtPODtl.Rows[i][11]);//3200; //pod_ord_wgt
                                objDstbType.orderedWeightType = BusinessUtility.GetString(dtPODtl.Rows[i][12]); //"A"; //pod_ord_wgt_typ
                                if (objDstbType.orderedWeightType == "A")
                                    objDstbType.orderedWeightSpecified = true;
                                objDstbType.transitDays = BusinessUtility.GetInt(dtPODtl.Rows[i][13]); //pod_trnst_dy
                                //pod_frt_cst
                                if (objDstbType.freightCost > 0)
                                {
                                    objDstbType.freightCostUnitOfMeasure = BusinessUtility.GetString(dtPODtl.Rows[i][15]);  // Mandatory if freightCost is entered //pod_frt_cst_um
                                    objDstbType.freightVendorId = BusinessUtility.GetString(dtPODtl.Rows[i][16]); ; // Mandatory if freightCost is entered //pod_frt_ven_id 
                                    objDstbType.freightCurrencyCode = BusinessUtility.GetString(dtPODtl.Rows[i][17]); // Mandatory if freightCost is entered //pod_frt_cry
                                    objDstbType.freightExchangeRate = BusinessUtility.GetDecimal(dtPODtl.Rows[i][18]); // Mandatory if freightCost is entered //pod_frt_exrt 
                                    objDstbType.freightExchangeRateType = BusinessUtility.GetString(dtPODtl.Rows[i][19]); // Mandatory if freightCost is entered //pod_frt_ex_rt_typ
                                }

                                //Read FRTFRT-REC
                                //objDstbType.freightCostUnitOfMeasure = "LBS";  // Mandatory if freightCost is entered //pod_frt_cst_um
                                //objDstbType.freightVendorId = "28" ; // Mandatory if freightCost is entered //pod_frt_ven_id 
                                //objDstbType.freightCurrencyCode = "USD"; // Mandatory if freightCost is entered //pod_frt_cry
                                //objDstbType.freightCost = 0.1800M;
                                //objDstbType.freightExchangeRate = 1;
                                //objDstbType.freightExchangeRateType = "V";

                                objDstbType.inquiryCostUpdated = BusinessUtility.GetString(dtPODtl.Rows[i][21]); //"A"; //pod_inq_upd
                                if (objDstbType.inquiryCostUpdated == "M")
                                    objDstbType.inquiryCost = BusinessUtility.GetDecimal(dtPODtl.Rows[i][20]);//4.02M;// Only set this value when inquiryCostUpdated = "M" //pod_inq_cst

                                objDstbType.inquiryCostSpecified = true;
                                objDstbType.inquiryRemark = BusinessUtility.GetString(dtPODtl.Rows[i][22]);  //pod_inq_rmk


                                //PurchaseOrderItemService.AuthenticationToken oAuthenticationToken = new PurchaseOrderItemService.AuthenticationToken();
                                //oAuthenticationToken.username = oCreateWithAutoNumberingResponse.AuthenticationHeader.username;
                                //oAuthenticationToken.value = oCreateWithAutoNumberingResponse.AuthenticationHeader.value;




                                OriginZones oOriginZones = new OriginZones();

                                PurchaseOrderItemService.CreateRequest oCreateRequest =
                                    new PurchaseOrderItemService.CreateRequest(oAuthenticationToken, oCreateWithManualNumberingResponse.purchaseOrderIdentity, objPOItmType, objRlsType,
                                        objDstbType, oOriginZones, objPrsDrcType);

                                PurchaseOrderItemService.CreateResponse oCreateResponse = new PurchaseOrderItemService.CreateResponse();

                                PurchaseOrderItemService.PurchaseOrderItemService oPurchaseOrderItemServiceClient = new PurchaseOrderItemService.PurchaseOrderItemServiceClient();

                                try
                                {
                                    oCreateResponse = oPurchaseOrderItemServiceClient.Create(oCreateRequest);
                                    oAuthenticationToken = new PurchaseOrderItemService.AuthenticationToken();
                                    oAuthenticationToken.username = oCreateResponse.AuthenticationHeader.username;
                                    oAuthenticationToken.value = oCreateResponse.AuthenticationHeader.value;

                                    objPOHdrAuth = new PurchaseOrderHeaderService.AuthenticationToken();
                                    objPOHdrAuth.username = oCreateResponse.AuthenticationHeader.username;
                                    objPOHdrAuth.value = oCreateResponse.AuthenticationHeader.value;

                                    ErrorLog.createLog("POIteam Token: " + oCreateResponse.AuthenticationHeader.value + "\n POHeader Token User: " + oCreateResponse.AuthenticationHeader.username
                                 + "\n" + "PO Header Identity:" + oCreateResponse.purchaseOrderItemIdentity);
                                    //Transaction completed // Ordered item is created
                                    ErrorLog.createLog("Purchase Order item created successfully PO Item Identity: " + oCreateResponse.purchaseOrderItemIdentity + " Original PO No is " + BusinessUtility.GetString(drPOH[0]));

                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.createLog("Error during creating POItem,PORls,POtpod and tctipd table for POHeader Iendtity: " + oCreateWithManualNumberingResponse.purchaseOrderIdentity + "\n" + "Error:" + ex.Message);
                                    ErrorLog.createLog("Original PO No: " + BusinessUtility.GetString(drPOH[0]) + " Which is not inserted ");

                                     //sctmsg_rec table 
                                try
                                {
                                    dtPOError = objPOTrans.PurchaseOrderError();
                                }
                                catch (Exception exError)
                                {
                                    ErrorLog.createLog("Error during geting Error data from sctmsg_rec table from database " + ex.Message);
                                   // throw;
                                }

                                for (int iRow = 0; iRow < dtPOError.Rows.Count; iRow++  )
                                {
                                    ErrorLog.createLog("Error Line " + iRow + " " + BusinessUtility.GetString(dtPOError.Rows[iRow][3]) + "\t Error Type: " + BusinessUtility.GetString(dtPOError.Rows[iRow][10]) + "\t Error Msg:" + BusinessUtility.GetString(dtPOError.Rows[iRow][11]));
                                }

                                    //PurchaseOrderItemService.CloseRequest objClsRqs = new PurchaseOrderItemService.CloseRequest();
                                    //PurchaseOrderItemService.CloseResponse objClsRsp = new PurchaseOrderItemService.CloseResponse();

                                    i = dtPOItm.Rows.Count + 1;
                                    UserAuthenticationLogOut(iToken);
                                    GatewayAuthService.AuthenticationToken recurrentToken = new GatewayAuthService.AuthenticationToken();
                                    recurrentToken = UserAuthentication();

                                    objPOHdrAuth = new PurchaseOrderHeaderService.AuthenticationToken();
                                    objPOHdrAuth.username = recurrentToken.username;
                                    objPOHdrAuth.value = recurrentToken.value;
                                    iToken = recurrentToken;

                                }


                                string s = "stop;";

                            }// end For loop here
                        }//if condition end here
                        else
                        {
                            ErrorLog.createLog("No. of rows for tables potpoi, potpor, tctipd and potpod are different for PO No:  " + BusinessUtility.GetString(drPOH[0]));
                            objPOHdrAuth = new PurchaseOrderHeaderService.AuthenticationToken();
                            objPOHdrAuth.username = oAuthenticationToken.username;
                            objPOHdrAuth.value = oAuthenticationToken.value;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.createLog("Error during creating POHeader Table for PO No. " + BusinessUtility.GetString(drPOH[0]));
                        UserAuthenticationLogOut(iToken);
                        GatewayAuthService.AuthenticationToken recurrentToken = new GatewayAuthService.AuthenticationToken();
                        recurrentToken = UserAuthentication();
                        objPOHdrAuth.username = recurrentToken.username;
                        objPOHdrAuth.value = recurrentToken.value;
                        iToken = recurrentToken;
                        //oAuthenticationToken = new PurchaseOrderItemService.AuthenticationToken();
                        //oAuthenticationToken.username = oCreateWithManualNumberingResponse.AuthenticationHeader.username;
                        //oAuthenticationToken.value = oCreateWithManualNumberingResponse.AuthenticationHeader.value;
                    }
                } //Commented foreach PO Header loop end
               UserAuthenticationLogOut(iToken);
            }
            catch (Exception ex)
            {

                ErrorLog.createLog("Error during creating POHeader table " + ex.Message);
                throw;
            }
        }
    }
}