﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Text;

namespace CreatePO
{

    public static class ErrorLog
    {
        static string slogFilePath = ConfigurationManager.AppSettings["RootDiractory"] + "Log/";
        static string sFileName = "Log-" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
        static string sFilePath = slogFilePath + sFileName;

        public static void createLog(string errorMessage)
        {
            //  string path = ConfigurationManager.AppSettings["LogsDiractory"] + "Log" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            string path = ConfigurationManager.AppSettings["RootDiractory"] + "Log/Log" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";

            // if (BusinessUtility.GetString(ConfigurationManager.AppSettings["LogError"]).ToString().ToUpper() == "TRUE")
            // {
            if (!File.Exists(path))
            {
                StreamWriter sw = File.CreateText(path);
                sw.Close();
            }
            using (System.IO.StreamWriter sw = System.IO.File.AppendText(path))
            {
                sw.WriteLine("-------- " + DateTime.Now + " --------");
                sw.WriteLine(errorMessage);
                sw.WriteLine("------------------------");
                sw.Close();
            }
            // }
        }

        public static void CreateLog(Exception ex)
        {
            if (!Directory.Exists(slogFilePath))
            {
                Directory.CreateDirectory(slogFilePath);
            }
            if (!File.Exists(sFilePath))
            {
                var objFile = File.Create(sFilePath);
                objFile.Close();
            }



            StringBuilder sbLogText = new StringBuilder("Log starts :" + DateTime.Now);
            sbLogText.AppendLine();
            sbLogText.AppendLine();
            sbLogText.Append(ex.ToString());
            sbLogText.AppendLine();
            sbLogText.AppendLine();
            sbLogText.Append("Log ends :" + DateTime.Now);
            sbLogText.AppendLine();
            sbLogText.AppendLine();
            sbLogText.AppendLine();
            bool bAppend = true;
            StreamWriter oStreamWriter = new StreamWriter(sFilePath, bAppend);
            oStreamWriter.Write(sbLogText);
            oStreamWriter.Close();
        }
    }

}
