﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using iTECH.Library.DataAccess.ODBC;
using System.Configuration;
using System.Data.Odbc;

namespace CreatePO
{

    public class POTransaction
    {
        //For testing 
        //public DataTable PurchaseOrderItemData(string poNumber)
        //{
        //    DataTable dtResult = new DataTable();
        //    DbHelper dbhelper = new DbHelper();
        //    try
        //    {
        //        StringBuilder sqlSelect = new StringBuilder();
        //        sqlSelect.Append(" select poi_ord_wgt_um, poi_bkt_pcs, poi_bkt_msr, poi_bkt_wgt, poi_bkt_qty, poi_bkt_wgt_um,  poi_consg, poi_pur_cat, poi_src, poi_alw_prtl_shp, ");
        //        sqlSelect.Append(" poi_ovrshp_pct, poi_undshp_pct, poi_invt_qlty, poi_aly_schg_apln, poi_dsgd_shp_whs, poi_ven_prc_src, poi_prc_in_eff from potpoi_rec where (select count(*) ");
        //        sqlSelect.Append(" from potpod_rec where pod_cmpy_id = poi_cmpy_id and pod_po_pfx = poi_po_pfx and pod_po_no = " + poNumber + " and pod_po_itm = poi_po_itm and  pod_trcomp_sts <> 'C') > 0 and poi_po_no = " + poNumber + " ");
        //        dtResult = dbhelper.GetDataTable(sqlSelect.ToString(), CommandType.Text);

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.CreateLog(ex);
        //        throw;
        //    }
        //    finally
        //    {
        //        dbhelper.CloseDatabaseConnection();
        //    }
        //    return dtResult;
        //}


        public DataTable PurchaseHeaderData()
        {
            DataTable dtResult = new DataTable();
            DbHelper dbhelper = new DbHelper();
            try
            {
                StringBuilder sqlSelect = new StringBuilder();
                sqlSelect.Append(" select poh_po_no, poh_po_brh, poh_ven_id, poh_shp_fm, poh_mill, poh_po_typ, poh_inv_ml_to_brh, poh_pur_pttrm, poh_pur_disc_trm, poh_byr, poh_cry, ");
                sqlSelect.Append(" poh_exrt, poh_ex_rt_typ, poh_shp_itm_tgth, poh_ack_reqd, poh_po_pl_dt, (select count(*) from frtfrt_rec where frt_ref_pfx = poh_po_pfx and frt_ref_no=poh_po_no and frt_ref_itm = 0) as isFreight  from potpoh_rec ");
                sqlSelect.Append(" WHERE (select count(*) from potpod_rec where pod_cmpy_id = poh_cmpy_id and pod_po_pfx = poh_po_pfx and pod_po_no = poh_po_no and pod_trcomp_sts <> 'C') > 0 ");
                sqlSelect.Append(" and poh_po_no in ('92058','92161','92453','92455','92738','92741','92744','92745')");
                //sqlSelect.Append(" and poh_po_no in ('92058','92161','92453','92455','92738','92741','92744','92745')"); 
                dtResult = dbhelper.GetDataTable(sqlSelect.ToString(), CommandType.Text);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbhelper.CloseDatabaseConnection();
            }
            return dtResult;
        }

        public DataTable PurchaseOrderItemData(string poNumber)
        {
            DataTable dtResult = new DataTable();
            DbHelper dbhelper = new DbHelper();
            try
            {
                StringBuilder sqlSelect = new StringBuilder();
                sqlSelect.Append(" select poi_ord_wgt_um, poi_bkt_pcs, poi_bkt_msr, poi_bkt_wgt, poi_bkt_qty, poi_bkt_wgt_um,  poi_consg, poi_pur_cat, poi_src, poi_alw_prtl_shp, ");
                sqlSelect.Append(" poi_ovrshp_pct, poi_undshp_pct, poi_invt_qlty, poi_aly_schg_apln, poi_dsgd_shp_whs, poi_ven_prc_src, poi_prc_in_eff,poi_po_itm from potpoi_rec where (select count(*) ");
                sqlSelect.Append(" from potpod_rec where pod_cmpy_id = poi_cmpy_id and pod_po_pfx = poi_po_pfx and pod_po_no = " + poNumber + " and pod_po_itm = poi_po_itm and  pod_trcomp_sts <> 'C') > 0 and poi_po_no = " + poNumber + " ");
                dtResult = dbhelper.GetDataTable(sqlSelect.ToString(), CommandType.Text);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbhelper.CloseDatabaseConnection();
            }
            return dtResult;
        }

        public DataTable PurchaseOrderRlsData(string poNumber)
        {
            DataTable dtResult = new DataTable();
            DbHelper dbhelper = new DbHelper();
            try
            {
                StringBuilder sqlSelect = new StringBuilder();
                sqlSelect.Append(" select por_po_intl_rls_no, por_po_rls_no, por_rdy_pcs, por_rdy_msr, por_rdy_wgt, por_rdy_qty, por_rdy_dt, por_due_dt_qlf, por_due_dt_fmt, ");
                sqlSelect.Append(" por_due_dt_ent,  por_due_fm_dt, por_due_to_dt, por_due_dt_wk_no, por_due_dt_wk_cy,  por_roll_sch, por_ack_rcvd, por_ack_dt from potpor_rec ");
                sqlSelect.Append(" where (select count(*) from potpod_rec where pod_cmpy_id = por_cmpy_id and pod_po_pfx = por_po_pfx and pod_po_no = " + poNumber + " ");
                sqlSelect.Append(" and pod_po_itm = por_po_itm and  pod_trcomp_sts <> 'C') > 0 and por_po_no = " + poNumber + " ");
                dtResult = dbhelper.GetDataTable(sqlSelect.ToString(), CommandType.Text);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbhelper.CloseDatabaseConnection();
            }
            return dtResult;
        }

        public DataTable PurchaseOrderDistData(string poNumber)
        {
            DataTable dtResult = new DataTable();
            DbHelper dbhelper = new DbHelper();
            try
            {
                StringBuilder sqlSelect = new StringBuilder();
                sqlSelect.Append(" select pod_po_intl_rls_no, pod_po_dist, pod_dist_typ, pod_bgt_for, pod_bgt_for_cus_id, pod_bgt_for_part, pod_bgt_for_slp, pod_bal_pcs as pod_ord_pcs, ");
                sqlSelect.Append(" pod_ord_pcs_typ, pod_bal_msr as pod_ord_msr, pod_ord_msr_typ, pod_bal_wgt as pod_ord_wgt, pod_ord_wgt_typ, pod_trnst_dy, pod_frt_cst, pod_frt_cst_um, pod_frt_ven_id, ");
                sqlSelect.Append(" pod_frt_cry, pod_frt_exrt, pod_frt_ex_rt_typ, pod_inq_cst, pod_inq_upd, pod_inq_rmk from potpod_rec  where pod_trcomp_sts <> 'C' and pod_po_no = " + poNumber + " ");
                dtResult = dbhelper.GetDataTable(sqlSelect.ToString(), CommandType.Text);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbhelper.CloseDatabaseConnection();
            }
            return dtResult;
        }

        public DataTable PurchaseOrderPrdItm(string poNumber)
        {
            DataTable dtResult = new DataTable();
            DbHelper dbhelper = new DbHelper();
            try
            {
                StringBuilder sqlSelect = new StringBuilder();
                sqlSelect.Append(" select ipd_frm, ipd_grd, ipd_num_size1, ipd_num_size2, ipd_num_size3, ipd_num_size4, ipd_num_size5, ipd_size, ipd_fnsh, ipd_wdth, ipd_lgth, ");
                sqlSelect.Append(" ipd_dim_dsgn, ipd_idia, ipd_odia, ipd_ga_size, ipd_ga_typ, ipd_alt_size, ipd_rdm_dim_1, ipd_rdm_dim_2, ipd_rdm_dim_3, ipd_rdm_dim_4, ");
                sqlSelect.Append(" ipd_rdm_dim_5, ipd_rdm_dim_6, ipd_rdm_dim_7, ipd_rdm_dim_8, ipd_rdm_area, ipd_cus_ven_id, ipd_ent_msr, ipd_frc_fmt, ipd_ord_lgth_typ, ");
                sqlSelect.Append(" ipd_fmt_desc_frc, ipd_fmt_size_desc, ipd_part_cus_id, ipd_part, ipd_part_rev_no, ipd_part_acs, ipd_part_ctl_no, ipd_grs_wdth, ipd_grs_lgth, ");
                sqlSelect.Append(" ipd_grs_pcs, ipd_grs_wdth_intgr, ipd_grs_wdth_nmr, ipd_grs_wdth_dnm, ipd_grs_lgth_intgr, ipd_grs_lgth_nmr, ipd_grs_lgth_dnm, ipd_wdth_intgr, ");
                sqlSelect.Append(" ipd_wdth_nmr, ipd_wdth_dnm, ipd_lgth_intgr, ipd_lgth_nmr, ipd_lgth_dnm, ipd_idia_intgr, ipd_idia_nmr, ipd_idia_dnm, ipd_odia_intgr, ");
                sqlSelect.Append(" ipd_odia_nmr, ipd_odia_dnm, ipd_cstm_sha  from tctipd_rec join potpod_rec on ipd_cmpy_id = pod_cmpy_id and ipd_ref_pfx = pod_po_pfx ");
                sqlSelect.Append(" and pod_po_no = " + poNumber + " and ipd_ref_itm = pod_po_itm and pod_trcomp_sts <> 'C' and ipd_ref_no =  " + poNumber + "  ");

                dtResult = dbhelper.GetDataTable(sqlSelect.ToString(), CommandType.Text);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbhelper.CloseDatabaseConnection();
            }
            return dtResult;
        }

        public DataTable PurchaseOrderFreight(string poNumber)
        {
            DataTable dtResult = new DataTable();
            DbHelper dbhelper = new DbHelper();
            try
            {

                StringBuilder sqlSelect = new StringBuilder();
                sqlSelect.Append(" select frt_dlvy_mthd, frt_trm_trd, frt_crr_nm, frt_cst, frt_cst_um, frt_frt_ven_id, frt_cry, frt_exrt, frt_ex_rt_typ ");
                sqlSelect.Append(" from frtfrt_rec Where frt_ref_pfx = 'PO' and frt_ref_no = " + poNumber + " and frt_ref_itm = 0 ");

                dtResult = dbhelper.GetDataTable(sqlSelect.ToString(), CommandType.Text);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbhelper.CloseDatabaseConnection();
            }
            return dtResult;
        }

        public DataTable PurchaseOrderItmCost(string poNumber, string poItm)
        {
            DataTable dtResult = new DataTable();
            DbHelper dbhelper = new DbHelper();
            try
            {
                StringBuilder sqlSelect = new StringBuilder();
                sqlSelect.Append(" Select csi_cst_no, csi_cst_desc20,csi_cst_cl,csi_cst,csi_cst_um from cttcsi_rec where csi_ref_pfx = 'PO' and csi_ref_no = " + poNumber + "  ");
                sqlSelect.Append(" and csi_ref_itm = " + poItm + "  and  csi_cc_typ = 1 ");

                dtResult = dbhelper.GetDataTable(sqlSelect.ToString(), CommandType.Text);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbhelper.CloseDatabaseConnection();
            }
            return dtResult;
        }

        public DataTable PurchaseOrderMtlStd(string poNumber, string poItm)
        {
            DataTable dtResult = new DataTable();
            DbHelper dbhelper = new DbHelper();
            try
            {
                StringBuilder sqlSelect = new StringBuilder();
                sqlSelect.Append(" select mss_sdo, mss_std_id, mss_addnl_id from pntssp_rec  join mcrmss_rec on mss_mss_ctl_no = ssp_mss_ctl_no where ssp_ref_pfx = 'PO' ");
                sqlSelect.Append(" and ssp_ref_no = " + poNumber + " and ssp_ref_itm = " + poItm + " ");

                dtResult = dbhelper.GetDataTable(sqlSelect.ToString(), CommandType.Text);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbhelper.CloseDatabaseConnection();
            }
            return dtResult;
        }

        public DataTable PurchaseOrderHdrInstruction(string poNumber)
        {
            DataTable dtResult = new DataTable();
            DbHelper dbhelper = new DbHelper();
            try
            {
                StringBuilder sqlSelect = new StringBuilder();
                sqlSelect.Append(" select tsi_cmpy_id, tsi_ref_pfx, tsi_ref_no, tsi_ref_itm, tsi_ref_sbitm, tsi_rmk_typ, tsi_lng, tsi_lib_id, tsi_txt from tcttsi_rec ");
                sqlSelect.Append(" where tsi_cmpy_id = 'PSS' and tsi_ref_pfx = 'PO' and tsi_ref_no  = " + poNumber + " and tsi_ref_itm = 0 ");

                dtResult = dbhelper.GetDataTable(sqlSelect.ToString(), CommandType.Text);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbhelper.CloseDatabaseConnection();
            }
            return dtResult;
        }

        public DataTable PurchaseOrderItmInstruction(string poNumber, string poItm)
        {
            DataTable dtResult = new DataTable();
            DbHelper dbhelper = new DbHelper();
            try
            {
                StringBuilder sqlSelect = new StringBuilder();
                sqlSelect.Append(" select tsi_cmpy_id, tsi_ref_pfx, tsi_ref_no, tsi_ref_itm, tsi_ref_sbitm, tsi_rmk_typ, tsi_lng, tsi_lib_id, tsi_txt from tcttsi_rec ");
                sqlSelect.Append(" where tsi_cmpy_id = 'PSS' and tsi_ref_pfx = 'PO' and tsi_ref_no  = " + poNumber + " and tsi_ref_itm =  " + poItm + "  ");

                dtResult = dbhelper.GetDataTable(sqlSelect.ToString(), CommandType.Text);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                dbhelper.CloseDatabaseConnection();
            }
            return dtResult;
        }

        public DataTable PurchaseOrderError()
        {
            DataTable dtResult = new DataTable();
            string conStx = ConfigurationManager.ConnectionStrings["NewConnectionString1"].ConnectionString;
            OdbcConnection con = new OdbcConnection(conStx);
            con.Open();
            //DbHelper dbhelper = new DbHelper("conStx");
            try
            {
                StringBuilder sqlSelect = new StringBuilder();
                sqlSelect.Append(" SElect first 5 * from sctmsg_rec order by 4 desc; ");
                OdbcDataAdapter da = new OdbcDataAdapter(sqlSelect.ToString(), con);
                da.Fill(dtResult);
                //dtResult = dbhelper.GetDataTable(sqlSelect.ToString(), CommandType.Text);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw;
            }
            finally
            {
                con.Close();
               // dbhelper.CloseDatabaseConnection();
            }
            return dtResult;
        }
    }

}

